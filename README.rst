===============
Djamon Protocol
===============

Google Protobuf protocol files for Djamon.

Requirements
------------

You will need to install ``protoc`` at least version 2.5.0. On Ubuntu this is in apt as
the ``protobuf-compiler`` package.

To run the ``Makefile`` you will need some version of ``make``.

Compiling
---------

Simply run ``make`` in the checkout directory. This will create the required Python
files for importing.

