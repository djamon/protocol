all: __init__.py djamon_pb2.py

djamon_pb2.py: djamon.proto
	protoc -I=. --python_out=. djamon.proto

__init__.py:
	touch __init__.py

clean:
	rm -f *.pyc *.py

.PHONY: clean all

